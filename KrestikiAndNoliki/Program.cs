﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KrestikiAndNoliki
{
    class Program
    {

        static void Main(string[] args)
        {
            #region Переменные
            const int N = 3;
            char[,] mas = new char[N, N];

            bool play = true;

            int step = 0;
            int ii, jj, StepPole = 9, 
                U1 = 0, U2 = 0, U3 = 0, U4 = 0, U5 = 0, U6 = 0, U7 = 0, U8 = 0,
                B1 = 0, B2 = 0, B3 = 0, B4 = 0, B5 = 0, B6 = 0, B7 = 0, B8 = 0;

            Random rnd = new Random();
            #endregion

            #region Обнуление полей
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    mas[i, j] = '.';

                }
            }
            #endregion

            #region Игровой цикл
            while (play == true)
            {
                #region Очистить экран и вывести игровае поле
                Console.Clear();
                for (int i = 0; i < N; i++)
                {
                    for (int j = 0; j < N; j++)
                    {
                        Console.Write(mas[i, j]);
                    }
                    Console.WriteLine();
                }
                #endregion

                #region Определение кто ходит и ход
                step++;
                if (step % 2 == 1)
                {
                    #region Ход игрока
                    Console.WriteLine("Ход игрока");
                    do
                    {
                        Console.Write("Введите строку: ");
                        ii = int.Parse(Console.ReadLine()) - 1;

                        Console.Write("Введите столбец: ");
                        jj = int.Parse(Console.ReadLine()) - 1;
                    }
                    while (ii < 0 || jj < 0 || ii > N - 1 || jj > N - 1 ||
                    mas[ii, jj] == 'O' || mas[ii, jj] == 'X');

                    mas[ii, jj] = 'X';

                    StepPole--;

                    for (int i = 0; i < N; i++)
                    {
                        for (int j = 0; j < N; j++)
                        {
                            if (i == j)
                            {
                                if (mas[i, j] == 'X' && U1 != 1)
                                {
                                    U1++;
                                }
                            }
                            if (i + j == 2)
                            {
                                if (mas[i, j] == 'X' && U2 != 1)
                                {
                                    U2++;
                                }
                            }
                            if (i == 0)
                            {
                                if (mas[i, j] == 'X' && U3 != 1)
                                {
                                    U3++;
                                }
                            }
                            if (i == 1)
                            {
                                if (mas[i, j] == 'X' && U4 != 1)
                                {
                                    U4++;
                                }
                            }
                            if (i == 2)
                            {
                                if (mas[i, j] == 'X' && U5 != 1)
                                {
                                    U5++;
                                }
                            }
                            if (j == 0)
                            {
                                if (mas[i, j] == 'X' && U6 != 1)
                                {
                                    U6++;
                                }
                            }
                            if (j == 1)
                            {
                                if (mas[i, j] == 'X' && U7 != 1)
                                {
                                    U7++;
                                }
                            }
                            if (j == 2)
                            {
                                if (mas[i, j] == 'X' && U8 != 1)
                                {
                                    U8++;
                                }
                            }
                        }
                    }
                    #endregion
                }
                else
                {
                    #region Ход компа
                    Console.WriteLine("Ход компа");
                    Console.WriteLine("Нажмите Enter");
                    Console.ReadKey();
                    do
                    {
                        ii = rnd.Next(0, N);
                        jj = rnd.Next(0, N);
                    }
                    while (mas[ii, jj] == 'X' || mas[ii, jj] == 'O');

                    mas[ii, jj] = 'O';

                    StepPole--;

                    for (int i = 0; i < N; i++)
                    {
                        for (int j = 0; j < N; j++)
                        {
                            if (i == j)
                            {
                                if (mas[i, j] == 'O' && B1 != 1)
                                {
                                    B1++;
                                }
                            }
                            if (i + j == 2)
                            {
                                if (mas[i, j] == 'O' && B2 != 1)
                                {
                                    B2++;
                                }
                            }
                            if (i == 0)
                            {
                                if (mas[i, j] == 'O' && B3 != 1)
                                {
                                    B3++;
                                }
                            }
                            if (i == 1)
                            {
                                if (mas[i, j] == 'O' && B4 != 1)
                                {
                                    B4++;
                                }
                            }
                            if (i == 2)
                            {
                                if (mas[i, j] == 'O' && B5 != 1)
                                {
                                    B5++;
                                }
                            }
                            if (j == 0)
                            {
                                if (mas[i, j] == 'O' && B6 != 1)
                                {
                                    B6++;
                                }
                            }
                            if (j == 1)
                            {
                                if (mas[i, j] == 'O' && B7 != 1)
                                {
                                    B7++;
                                }
                            }
                            if (j == 2)
                            {
                                if (mas[i, j] == 'O' && B8 != 1)
                                {
                                    B8++;
                                }
                            }
                        }
                    }
                    #endregion
                }
                #endregion

                #region Проверка на окончание игры
                if (StepPole == 0 || U1 == 3 || U2 == 3 || U3 == 3 || U4 == 3 || U5 == 3 || U6 == 3 || U7 == 3 || U8 == 3 ||
                B1 == 3 || B2 == 3 || B3 == 3 || B4 == 3 || B5 == 3 || B6 == 3 || B7 == 3 || B8 == 3 )
                {
                    play = false;
                }
                #endregion
            }
            #endregion

            #region Очистить экран и вывести игровае поле
            Console.Clear();
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    Console.Write(mas[i, j]);
                }
                Console.WriteLine();
            }
            #endregion

            #region Победитель
            if (B1 == 3 || B2 == 3 || B3 == 3 || B4 == 3 || B5 == 3 || B6 == 3 || B7 == 3 || B8 == 3)
            {
                Console.WriteLine("Компьютер победил");
            }
            else if (U1 == 3 || U2 == 3 || U3 == 3 || U4 == 3 || U5 == 3 || U6 == 3 || U7 == 3 || U8 == 3)
            {
                Console.WriteLine("Игрок победил");
            }
            else
            {
                Console.WriteLine("Ничья");
            }
            #endregion


            Console.ReadKey();
        }
    }
}
